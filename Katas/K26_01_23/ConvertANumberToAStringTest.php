<?php

namespace Katas\K26_01_23;

use PHPUnit\Framework\TestCase;

function numberToString($num): string
{
    return (string)$num;
}

class ConvertANumberToAStringTest extends TestCase
{
    public function testSampleTests() {
        $this->assertSame('67', numberToString(67));
    }
}