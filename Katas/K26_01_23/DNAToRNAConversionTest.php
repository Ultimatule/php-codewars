<?php

namespace Katas\K26_01_23;

use PHPUnit\Framework\TestCase;

function dnaToRna($str): string {
    return str_replace('T', 'U', $str);
}

class DNAToRNAConversionTest extends TestCase {
    public function testFixedTests() {
        $this->assertSame('UUUU', dnaToRna("TTTT"));
        $this->assertSame('GCAU', dnaToRna("GCAT"));
        $this->assertSame("", dnaToRna(""));
        $this->assertSame('U', dnaToRna("T"));
        $this->assertSame('GACCGCCGCC', dnaToRna("GACCGCCGCC"));
        $this->assertSame('GAUUCCACCGACUUCCCAAGUACCGGAAGCGCGACCAACUCGCACAGC', dnaToRna("GATTCCACCGACTTCCCAAGTACCGGAAGCGCGACCAACTCGCACAGC"));
        $this->assertSame('CACGACAUACGGAGCAGCGCACGGUUAGUACAGCUGUCGGUGAACUCCAUGACA', dnaToRna("CACGACATACGGAGCAGCGCACGGTTAGTACAGCTGTCGGTGAACTCCATGACA"));
        $this->assertSame('CACGACAUACGGAGCAGCGCACGGUUAGUACAGCUGUCGGUGAACUCCAUGACA', dnaToRna("CACGACATACGGAGCAGCGCACGGTTAGTACAGCTGTCGGTGAACTCCATGACA"));
        $this->assertSame('AACCCUGUCCACCAGUAACGUAGGCCGACGGGAAAAAUAAACGAUCUGUCAAUG', dnaToRna("AACCCTGTCCACCAGTAACGTAGGCCGACGGGAAAAATAAACGATCTGTCAATG"));
    }
}