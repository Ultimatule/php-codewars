<?php

namespace Katas\K31_01_23;

use PHPUnit\Framework\TestCase;

/*
Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.

Example
createPhoneNumber([1,2,3,4,5,6,7,8,9,0]); // => returns "(123) 456-7890"
The returned format must be correct in order to complete this challenge.

Don't forget the space after the closing parentheses!

function createPhoneNumber($numbersArray) {
    // your code here
}


class MyTest extends TestCase {
  public function testBasicTests() {
    $this->assertSame('(123) 456-7890', createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]));
    $this->assertSame('(111) 111-1111', createPhoneNumber([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]));
  }
}

*/


function createPhoneNumber(array $numbersArray): string
{
    return sprintf("(%s%s%s) %s%s%s-%s%s%s%s", ...$numbersArray);
}


class CreatePhoneNumberTest extends TestCase
{
    public function testBasicTests()
    {
        $this->assertSame('(123) 456-7890', createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]));
        $this->assertSame('(111) 111-1111', createPhoneNumber([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]));
    }
}
