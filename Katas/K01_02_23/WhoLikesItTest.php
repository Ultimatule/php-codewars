<?php

namespace Katas\K01_02_23;

use PHPUnit\Framework\TestCase;

/**
 * You probably know the "like" system from Facebook and other pages. People can "like" blog posts, pictures or other items. We want to create the text that should be displayed next to such an item.
 *
 * Implement the function which takes an array containing the names of people that like an item. It must return the display text as shown in the examples:
 *
 * []                                -->  "no one likes this"
 * ["Peter"]                         -->  "Peter likes this"
 * ["Jacob", "Alex"]                 -->  "Jacob and Alex like this"
 * ["Max", "John", "Mark"]           -->  "Max, John and Mark like this"
 * ["Alex", "Jacob", "Mark", "Max"]  -->  "Alex, Jacob and 2 others like this"
 * Note: For 4 or more names, the number in "and 2 others" simply increases.
 *
 * function likes( $names ) {
 *
 * // Your code here...
 * }
 *
 * class ExampleTestCases extends TestCase {
 *
 * public function testReturnCorrectText() {
 *
 * $this->assertSame( 'no one likes this', likes( [] ) );
 * $this->assertSame( 'Peter likes this', likes( [ 'Peter' ] ) );
 * $this->assertSame( 'Jacob and Alex like this', likes( [ 'Jacob', 'Alex' ] ) );
 * $this->assertSame( 'Max, John and Mark like this', likes( [ 'Max', 'John', 'Mark' ]) );
 * $this->assertSame( 'Alex, Jacob and 2 others like this', likes( [ 'Alex', 'Jacob', 'Mark', 'Max' ] ) );
 * }
 * }
 */

function likes(array $names): string
{
    switch (count($names)) {
        case 0:
            return 'no one likes this';
        case 1:
            return sprintf("%s likes this", ...$names);
        case 2:
            return sprintf("%s and %s like this", ...$names);
        case 3:
            return sprintf("%s, %s and %s like this", ...$names);
        default:
            return sprintf("%s, %s and %d others like this", $names[0], $names[1], count($names)-2);
    }
}

class WhoLikesItTest extends TestCase
{

    public function testReturnCorrectText()
    {

        $this->assertSame('no one likes this', likes([]));
        $this->assertSame('Peter likes this', likes(['Peter']));
        $this->assertSame('Jacob and Alex like this', likes(['Jacob', 'Alex']));
        $this->assertSame('Max, John and Mark like this', likes(['Max', 'John', 'Mark']));
        $this->assertSame('Alex, Jacob and 2 others like this', likes(['Alex', 'Jacob', 'Mark', 'Max']));
    }
}