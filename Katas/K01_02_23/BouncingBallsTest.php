<?php

namespace Katas\K01_02_23;

use PHPUnit\Framework\TestCase;

/**
 * A child is playing with a ball on the nth floor of a tall building. The height of this floor above ground level, h, is known.
 *
 * He drops the ball out of the window. The ball bounces (for example), to two-thirds of its height (a bounce of 0.66).
 *
 * His mother looks out of a window 1.5 meters from the ground.
 *
 * How many times will the mother see the ball pass in front of her window (including when it's falling and bouncing?
 *
 * Three conditions must be met for a valid experiment:
 * Float parameter "h" in meters must be greater than 0
 * Float parameter "bounce" must be greater than 0 and less than 1
 * Float parameter "window" must be less than h.
 * If all three conditions above are fulfilled, return a positive integer, otherwise return -1.
 *
 * Note:
 * The ball can only be seen if the height of the rebounding ball is strictly greater than the window parameter.
 *
 * Examples:
 * - h = 3, bounce = 0.66, window = 1.5, result is 3
 *
 * - h = 3, bounce = 1, window = 1.5, result is -1
 *
 * (Condition 2) not fulfilled).
 *
 * function bouncingBall($h, $bounce, $window) {
 * // your code
 * }
 *
 * class MyTestCases extends TestCase
 * {
 *
 * public function testBasic($input, $expected)
 * {
 * $this->assertSame($expected, digital_root($input), 'Digital root calculation failed!');
 * }
 *
 * public function basicTestDataProvider()
 * {
 * return [
 * [16, 7],
 * [195, 6],
 * [992, 2],
 * [999999999999, 9],
 * [167346, 9],
 * [10, 1],
 * [0, 0]
 * ];
 * }
 * }
 */

function bouncingBall($h, $bounce, $window): int
{
    $hNext = $h;
    $noValidConditionsStates = [
        0 > $h,
        0 > $bounce,
        1 < $bounce,
        $h <= $window,
    ];
    if (in_array(true, $noValidConditionsStates)) {
        return -1;
    }
    $q = $bounce;
    $n = 2;
    $result = 1;


//        xdebug_break();
    while (true) {
        $bCurrent = $h * pow($q, $n - 1);
        ++$n;
        if ($bCurrent <= $window) {
            break;
        }

        $result += 2;
    }

    return $result;
}

class BouncingBallsTest extends TestCase
{
    private function revTest($actual, $expected)
    {
        $this->assertSame($expected, $actual);
    }

    public function testBasics()
    {
//        $this->revTest(bouncingBall(3.0, 0.66, 1.5), 3);
//        $this->revTest(bouncingBall(30.0, 0.66, 1.5), 15);
//        $this->revTest(bouncingBall(10, 0.6, 10), -1);
        $this->revTest(bouncingBall(2, 0.5, 1), 1);
    }
}