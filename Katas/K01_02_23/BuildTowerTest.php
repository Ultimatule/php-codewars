<?php

namespace Katas\K01_02_23;

use PHPUnit\Framework\TestCase;

/**
 * [
 * "  *  ",
 * " *** ",
 * "*****"
 * ]
 * And a tower with 6 floors looks like this:
 *
* [
* "     *     ",
* "    ***    ",
* "   *****   ",
* "  *******  ",
* " ********* ",
* "***********"
* ]
 *
* function tower_builder(int $n): array {
* // Build your tower here
* }
 *
* class TowerBuilderTest extends TestCase {
* public function testBaseCase() {
* $this->assertSame(['*'], tower_builder(1));
* }
* public function testDescriptionExamples() {
* $this->assertSame([
* '  *  ',
* ' *** ',
* '*****'
* ], tower_builder(3));
* $this->assertSame([
* '     *     ',
* '    ***    ',
* '   *****   ',
* '  *******  ',
* ' ********* ',
* '***********'
* ], tower_builder(6));
* }
* }
 */

function tower_builder(int $floorsCount): array
{
    $whitespaceCountEachSide = $floorsCount - 1;
    $wildcardCount = 1;
    $tower = [];
    for ($i = 0; $i < $floorsCount; $i++) {
        $tower[] = sprintf("%s%s%s",
            str_repeat(' ', $whitespaceCountEachSide),
            str_repeat('*', $wildcardCount),
            str_repeat(' ', $whitespaceCountEachSide)
        );
        $whitespaceCountEachSide -= 1;
        $wildcardCount += 2;
    }
    return $tower;
}

class BuildTowerTest extends TestCase {
    public function testBaseCase() {
        $this->assertSame(['*'], tower_builder(1));
    }
    public function testDescriptionExamples() {
        $this->assertSame([
            '  *  ',
            ' *** ',
            '*****'
        ], tower_builder(3));
        $this->assertSame([
            '     *     ',
            '    ***    ',
            '   *****   ',
            '  *******  ',
            ' ********* ',
            '***********'
        ], tower_builder(6));
    }
}