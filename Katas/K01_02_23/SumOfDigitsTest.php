<?php

namespace Katas\K01_02_23;

use PHPUnit\Framework\TestCase;

/**
 * Digital root is the recursive sum of all the digits in a number.
 *
 * Given n, take the sum of the digits of n. If that value has more than one digit, continue reducing in this way until a single-digit number is produced. The input will be a non-negative integer.
 *
 * Examples
 * 16  -->  1 + 6 = 7
 * 942  -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
 * 132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
 * 493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2
 *
 * function digital_root($number): int
 * {
 * // place your solution here
 * }
 *
 * class MyTestCases extends TestCase
 * {
 *
 * public function testBasic($input, $expected)
 * {
 * $this->assertSame($expected, digital_root($input), 'Digital root calculation failed!');
 * }
 *
 * public function basicTestDataProvider()
 * {
 * return [
 * [16, 7],
 * [195, 6],
 * [992, 2],
 * [999999999999, 9],
 * [167346, 9],
 * [10, 1],
 * [0, 0]
 * ];
 * }
 * }
 */

function digital_root($number): int
{
    while (1 < strlen($number)) {
        $number = array_sum(str_split($number));
    }
    return $number;
}

class SumOfDigitsTest extends TestCase
{
    /**
     * @dataProvider basicTestDataProvider
     */
    public function testBasic($input, $expected)
    {
        $this->assertSame($expected, digital_root($input), 'Digital root calculation failed!');
    }

    public function basicTestDataProvider()
    {
        return [
            [16, 7],
            [195, 6],
            [992, 2],
            [999999999999, 9],
            [167346, 9],
            [10, 1],
            [0, 0]
        ];
    }
}