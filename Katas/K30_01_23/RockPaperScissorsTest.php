<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;

/*
Rock Paper Scissors
Let's play! You have to return which player won! In case of a draw return Draw!.

Examples(Input1, Input2 --> Output):

"scissors", "paper" --> "Player 1 won!"
"scissors", "rock" --> "Player 2 won!"
"paper", "paper" --> "Draw!"

function rpc ($p1, $p2) {
    #you code here
}


class rpcTest extends TestCase {
  private function dotest($p1, $p2, $exp) {
        $actual = rpc ($p1, $p2);
        //echo $exp == $actual;
        $this->assertSame($exp, $actual);
    }

    public function testrpcBasics() {
        $this->dotest("rock", "scissors", "Player 1 won!");
        $this->dotest("scissors", "rock", "Player 2 won!");
        $this->dotest("scissors", "scissors", "Draw!");
    }
}

*/

//$winMessageTemplate = 'Player %d won!';
//$p1WinMessage = sprintf($winMessageTemplate, 1);
//$p2WinMessage = sprintf($winMessageTemplate, 2);
//$rock = 'rock';
//$scissors = 'scissors';
//$paper = 'paper';
//
//$rules =[
//    playerRock1 => [playerPaper2 => 2, playerScissors => 1],
//    playerPaper1 => [],
//    playerScissors1 => []
//];
function paperCase1($p2): int
{
    return $p2 === 'scissors' ? 2 : 1;
}

function rockCase1($p2): int
{
    return $p2 === 'paper' ? 2 : 1;
}

function scissorsCase1($p2): int
{
    return $p2 === 'rock' ? 2 : 1;
}

function rpc($p1, $p2)
{
    if ($p1 === $p2) return 'Draw!';
    $result = 0;
    switch ($p1) {
        case 'paper':
            $result = paperCase1($p2);
            break;
        case 'rock':
            $result = rockCase1($p2);
            break;
        case 'scissors':
            $result = scissorsCase1($p2);
            break;
    }
    return sprintf("Player %s won!", $result);
}


class RockPaperScissorsTest extends TestCase
{
    private function dotest($p1, $p2, $exp)
    {
        $actual = rpc($p1, $p2);
        $this->assertSame($exp, $actual);
    }

    public function testrpcBasics()
    {
        $this->dotest("rock", "scissors", "Player 1 won!");
        $this->dotest("scissors", "rock", "Player 2 won!");
        $this->dotest("scissors", "scissors", "Draw!");
    }
}
