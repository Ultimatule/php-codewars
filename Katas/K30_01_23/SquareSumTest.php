<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Complete the square sum function so that it squares each number passed into it and then sums the results together.

For example, for [1, 2, 2] it should return 9 because 1^2 + 2^2 + 2^2 = 9.

function square_sum($numbers) : int {
  // Write your code here...
}

class SquareSumTest extends TestCase {
    public function testBasic() {
      $this->assertSame(square_sum([1,2]), 5);
      $this->assertSame(square_sum([0, 3, 4, 5]), 50);
      $this->assertSame(square_sum([]), 0);
      $this->assertSame(square_sum([-1,-2]), 5);
      $this->assertSame(square_sum([-1,0,1]), 2);
    }
}
*/

function square_sum($numbers) : int {
    return array_sum(array_map(function ($arrayElement) {
        return pow($arrayElement, 2);
    }, $numbers));
}

class SquareSumTest extends TestCase {
    public function testBasic() {
        $this->assertSame(square_sum([1,2]), 5);
        $this->assertSame(square_sum([0, 3, 4, 5]), 50);
        $this->assertSame(square_sum([]), 0);
        $this->assertSame(square_sum([-1,-2]), 5);
        $this->assertSame(square_sum([-1,0,1]), 2);
    }
}