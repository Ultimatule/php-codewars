<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;

/*
Given an array of integers as strings and numbers, return the sum of the array values as if all were numbers.

Return your answer as a number.

function sum_mix($a) {
  // Your code here
}

class SumMixTest extends TestCase {
  public function testExamples() {
    $this->assertSame(22, sum_mix([9, 3, '7', '3']));
    $this->assertSame(42, sum_mix(['5', '0', 9, 3, 2, 1, '9', 6, 7]));
    $this->assertSame(41, sum_mix(['3', 6, 6, 0, '5', 8, 5, '6', 2, '0']));
  }
}

*/



function sum_mix($a) {
    return array_sum($a);
}

class SumMixedArrayTest extends TestCase {
    public function testExamples() {
        $this->assertSame(22, sum_mix([9, 3, '7', '3']));
        $this->assertSame(42, sum_mix(['5', '0', 9, 3, 2, 1, '9', 6, 7]));
        $this->assertSame(41, sum_mix(['3', 6, 6, 0, '5', 8, 5, '6', 2, '0']));
    }
}
