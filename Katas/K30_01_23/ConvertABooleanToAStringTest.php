<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;

/*
Implement a function which convert the given boolean value into its string representation.

Note: Only valid inputs will be given.

function booleanToString($b) {
  // your code here
}


class MyTest extends TestCase {
  public function testFixedTests() {
    $this->assertSame("true", booleanToString(true));
    $this->assertSame("false", booleanToString(false));
  }
}

*/


function booleanToString($b) {
    return $b ? "true" : "false";
}


class ConvertABooleanToAStringTest extends TestCase {
    public function testFixedTests() {
        $this->assertSame("true", booleanToString(true));
        $this->assertSame("false", booleanToString(false));
    }
}
