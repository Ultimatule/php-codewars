<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;

/*
Make a function that will return a greeting statement that uses an input; your program should return, "Hello, <name> how are you doing today?".

[Make sure you type the exact thing I wrote or the program may not execute properly]

function greet($name) {
  // Good Luck (like you need it)
}

class MyTest extends TestCase
{
    public function testSampleTest() {
      $this->assertSame("Hello, Ryan how are you doing today?", greet('Ryan'));
    }
}
*/

function greet(string $name): string {
    return sprintf("Hello, %s how are you doing today?", $name);
}

class ReturningStringsTest extends TestCase
{
    public function testSampleTest() {
        $this->assertSame("Hello, Ryan how are you doing today?", greet('Ryan'));
    }
}