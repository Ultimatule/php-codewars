<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Given a string of digits, you should replace any digit below 5 with '0' and any digit 5 and above with '1'.
Return the resulting string.

Note: input will never be an empty string

function fake_bin(string $s): string {
  // Write your code here
}

class RemoveStringSpacesTest extends TestCase {
  public function testExamples() {
    $this->assertSame('8j8mBliB8gimjB8B8jlB', no_space('8 j 8   mBliB8g  imjB8B8  jl  B'));
    $this->assertSame('88Bifk8hB8BB8BBBB888chl8BhBfd', no_space('8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd'));
    $this->assertSame('8aaaaaddddr', no_space('8aaaaa dddd r     '));
  }
}
*/

function fake_bin(string $s): string
{
    $s = preg_replace('/[0-4]/', '0', $s);
    $s = preg_replace('/[5-9]/', '1', $s);
    return $s;
}

class FakeBinaryTest extends TestCase
{
    public function testExamples()
    {
        $this->assertSame('01011110001100111', fake_bin('45385593107843568'));
        $this->assertSame('101000111101101', fake_bin('509321967506747'));
        $this->assertSame('011011110000101010000011011', fake_bin('366058562030849490134388085'));
    }
}