<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;

/*
A square of squares
You like building blocks. You especially like building blocks that are squares. And what you even like more, is to arrange them into a square of square building blocks!

However, sometimes, you can't arrange them into a square. Instead, you end up with an ordinary rectangle! Those blasted things! If you just had a way to know, whether you're currently working in vain… Wait! That's it! You just have to check if your number of building blocks is a perfect square.

Task
Given an integral number, determine if it's a square number:

In mathematics, a square number or perfect square is an integer that is the square of an integer; in other words, it is the product of some integer with itself.

The tests will always use some integral number, so don't worry about that in dynamic typed languages.

Examples
-1  =>  false
 0  =>  true
 3  =>  false
 4  =>  true
25  =>  true
26  =>  false

function isSquare($n){
    return false; // fix me
}


class SumMixTest extends TestCase {
  public function testExamples() {
    $this->assertSame(22, sum_mix([9, 3, '7', '3']));
    $this->assertSame(42, sum_mix(['5', '0', 9, 3, 2, 1, '9', 6, 7]));
    $this->assertSame(41, sum_mix(['3', 6, 6, 0, '5', 8, 5, '6', 2, '0']));
  }
}

*/


function isSquare($n): bool
{
    if (0 > $n) return false;
    $squareRootOfNumber = sqrt($n);
    return 0 < $squareRootOfNumber - floor($squareRootOfNumber) === false;
}


class YoureASquareTest extends TestCase
{
    public function testShouldWorkForSomeExamples()
    {
        $this->assertFalse(isSquare(-1), "Negative numbers cannot be square numbers");
        $this->assertTrue(isSquare(0));
        $this->assertFalse(isSquare(3));
        $this->assertTrue(isSquare(4));
        $this->assertTrue(isSquare(25));
        $this->assertFalse(isSquare(26));
    }
}
