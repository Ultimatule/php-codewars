<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Simple, remove the spaces from the string, then return the resultant string.

function no_space(string $s): string {
  // Your code here
}

class RemoveStringSpacesTest extends TestCase {
  public function testExamples() {
    $this->assertSame('8j8mBliB8gimjB8B8jlB', no_space('8 j 8   mBliB8g  imjB8B8  jl  B'));
    $this->assertSame('88Bifk8hB8BB8BBBB888chl8BhBfd', no_space('8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd'));
    $this->assertSame('8aaaaaddddr', no_space('8aaaaa dddd r     '));
  }
}
*/

function no_space(string $s): string {
    return str_replace(" ", "", $s);
}

class RemoveStringSpacesTest extends TestCase {
    public function testExamples() {
        $this->assertSame('8j8mBliB8gimjB8B8jlB', no_space('8 j 8   mBliB8g  imjB8B8  jl  B'));
        $this->assertSame('88Bifk8hB8BB8BBBB888chl8BhBfd', no_space('8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd'));
        $this->assertSame('8aaaaaddddr', no_space('8aaaaa dddd r     '));
    }
}