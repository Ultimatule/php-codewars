<?php

namespace Katas\K30_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
You will be given an array a and a value x. All you need to do is check whether the provided array contains the value.

Array can contain numbers or strings. X can be either.

Return true if the array contains the value, false if not.

function solution($a, $x) {
}

class Test extends TestCase
{
    // test function names should start with "test"
    public function testThatSomethingShouldHappen() {
       $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "a"));
       $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "f"));
       $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "c"));
       $this->assertFalse(solution(array('a', 'b', 'c', 'd', 'e', "f"), "y"));
       $this->assertFalse(solution(array('a', 'b', 'c', 'd', 'e', "f"), null));
    }
}
*/

function solution(array $a, $x): bool {
    return in_array($x, $a);
}

class YouOnlyNeedOneBeginnerTest extends TestCase
{
    // test function names should start with "test"
    public function testThatSomethingShouldHappen() {
        $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "a"));
        $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "f"));
        $this->assertTrue(solution(array('a', 'b', 'c', 'd', 'e', "f"), "c"));
        $this->assertFalse(solution(array('a', 'b', 'c', 'd', 'e', "f"), "y"));
        $this->assertFalse(solution(array('a', 'b', 'c', 'd', 'e', "f"), null));
    }
}