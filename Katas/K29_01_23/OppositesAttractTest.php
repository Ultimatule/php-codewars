<?php

namespace Katas\K29_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
 * Timmy & Sarah think they are in love, but around where they live, they will only know once they pick a flower each.
 If one of the flowers has an even number of petals and the other has an odd number of petals it means they are in love.

Write a function that will take the number of petals of each flower and return true if they are in love and false if they aren't.

function lovefunc($flower1, $flower2) {
    // moment of truth
}

class MyTest extends TestCase
{
    public function testExample() {
        $this->assertTrue(lovefunc(1, 4));
        $this->assertFalse(lovefunc(2, 2));
        $this->assertTrue(lovefunc(0, 1));
        $this->assertFalse(lovefunc(0, 0));
    }
}*/

function lovefunc(int $flower1, int $flower2): bool
{
    return ($flower1 + $flower2) % 2 !== 0;
}

class OppositesAttractTest extends TestCase
{
    public function testExample()
    {
        $this->assertTrue(lovefunc(1, 4));
        $this->assertFalse(lovefunc(2, 2));
        $this->assertTrue(lovefunc(0, 1));
        $this->assertFalse(lovefunc(0, 0));
    }
}