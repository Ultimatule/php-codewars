<?php

namespace Katas\K29_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Write function RemoveExclamationMarks which removes all exclamation marks from a given string.

function remove_exclamation_marks($string) {
  // Your code here.
}

class RemoveExclamationMarksTestCase extends TestCase
{
    // test function names should start with "test"
    public function testExample() {
      $this->assertSame('Hello World', remove_exclamation_marks('Hello World!'));
    }
}*/

function remove_exclamation_marks($string): string {
    return str_replace('!', '', $string);
}

class RemoveExclamationMarksTest extends TestCase
{
    // test function names should start with "test"
    public function testExample() {
        $this->assertSame('Hello World', remove_exclamation_marks('Hello World!'));
    }
}