<?php

namespace Katas\K29_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Complete the solution so that it reverses the string passed into it.

'world'  =>  'dlrow'
'word'   =>  'drow'

function solution($str) {
  // Your code here
}

class ReversedStringsTest extends TestCase {
  public function testExamples() {
    $this->assertSame("dlrow", solution("world"));
    $this->assertSame("olleh", solution("hello"));
    $this->assertSame("", solution(""));
    $this->assertSame('h', solution("h"));
  }
}
*/

function solution(string $str): string {
    return strrev($str);
}

class ReversedStringsTest extends TestCase {
    public function testExamples() {
        $this->assertSame("dlrow", solution("world"));
        $this->assertSame("olleh", solution("hello"));
        $this->assertSame("", solution(""));
        $this->assertSame('h', solution("h"));
    }
}