<?php

namespace Katas\K29_01_23;

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\equalTo;

/*
Write a program that finds the summation of every number from 1 to num. The number will always be a positive integer greater than 0.

For example (Input -> Output):

2 -> 3 (1 + 2)
8 -> 36 (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)

function summation($n) {
  // Your code here
}

class SummationTest extends TestCase {
  public function testThatSummationWorksForExampleTests() {
    $this->assertSame(summation(1), 1);
    $this->assertSame(summation(2), 3);
    $this->assertSame(summation(3), 6);
    $this->assertSame(summation(4), 10);
    $this->assertSame(summation(5), 15);
  }
}
*/

function summation(int $n): int
{
    return array_sum(range(1, $n));
}

class GrasshopperSummationTest extends TestCase
{
    public function testThatSummationWorksForExampleTests()
    {
        $this->assertSame(summation(1), 1);
        $this->assertSame(summation(2), 3);
        $this->assertSame(summation(3), 6);
        $this->assertSame(summation(4), 10);
        $this->assertSame(summation(5), 15);
    }
}