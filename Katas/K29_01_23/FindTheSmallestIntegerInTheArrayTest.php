<?php

namespace Katas\K29_01_23;

use PHPUnit\Framework\TestCase;

/*
 *Given an array of integers your solution should find the smallest integer.

For example:

Given [34, 15, 88, 2] your solution will return 2
Given [34, -345, -1, 100] your solution will return -345
You can assume, for the purpose of this kata, that the supplied array will not be empty.

function smallestInteger ($arr) {
    #your code here
}

class smallest extends TestCase
{
    public function test() {
      $this->assertSame(smallestInteger([3, 5, 10, 1, 4, 55]), 1);
      $this->assertSame(smallestInteger([0]), 0);
    }
} */

/**
 * @param int[] $arr
 * @return int
 */
function smallestInteger(array $arr): int
{
    return min($arr);
}

class FindTheSmallestIntegerInTheArrayTest extends TestCase
{
    public function test()
    {
        $this->assertSame(smallestInteger([3, 5, 10, 1, 4, 55]), 1);
        $this->assertSame(smallestInteger([0]), 0);
    }
}